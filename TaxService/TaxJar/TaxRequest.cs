﻿using System.Collections.Generic;
using TaxService.DomainModels;

namespace TaxService.TaxJar
{
    internal class TaxRequest
    {
        public string FromCountry { get; set; }
        public string FromZip { get; set; }
        public string FromState { get; set; }
        public string FromCity { get; set; }
        public string FromStreet { get; set; }
        public string ToCountry { get; set; }
        public string ToZip { get; set; }
        public string ToState { get; set; }
        public string ToCity { get; set; }
        public string ToStreet { get; set; }
        public decimal? Amount { get; set; }
        public decimal Shipping { get; set; }
        public ExcemptionType? ExcemptionType { get; set; }
        public IEnumerable<LineItem> LineItems { get; set; }

        public TaxRequest(Order order)
        {
            FromCountry = order.FromAddress.Country;
            FromZip = order.FromAddress.Zip;
            FromState = order.FromAddress.State;
            FromCity = order.FromAddress.City;

            ToCountry = order.ToAddress.Country;
            ToZip = order.ToAddress.Zip;
            ToState = order.ToAddress.State;
            ToCity = order.ToAddress.City;

            Amount = order.Amount;
            Shipping = order.Shipping;

            ExcemptionType = order.ExcemptionType;

            LineItems = order.LineItems;
        }
    }
}
