﻿using TaxService.DomainModels;

namespace TaxService.TaxJar
{
    internal class RateResponse
    {
        public TaxRateInfo Rate { get; set; }
    }
}
