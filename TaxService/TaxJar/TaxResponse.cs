﻿using Newtonsoft.Json;
using System.Collections.Generic;
using TaxService.DomainModels;

namespace TaxService.TaxJar
{
    internal class TaxResponse
    {
        public Tax Tax { get; set; }
    }

    internal class Tax
    {
        public decimal AmountToCollect { get; set; }
    }
}


