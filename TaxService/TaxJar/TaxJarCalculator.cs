﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TaxService.DomainModels;
using TaxService.TaxJar;

namespace TaxService
{
    public class TaxJarCalculator : ITaxCalculator
    {
        private HttpClient apiClient;
        //private static DefaultContractResolver jsonResolver = new DefaultContractResolver
        //{
        //    NamingStrategy = new SnakeCaseNamingStrategy(),
        //};

        public TaxJarCalculator(Uri apiEndpoint, string apiKey)
        {
            apiClient = new HttpClient();
            apiClient.BaseAddress = apiEndpoint;
            apiClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            apiClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);

            JsonConvert.DefaultSettings = (() =>
            {
                var settings = new JsonSerializerSettings();

                var namingStrat = new SnakeCaseNamingStrategy();

                settings.Converters.Add(new StringEnumConverter { NamingStrategy = namingStrat });
                settings.ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = namingStrat
                };
                settings.NullValueHandling = NullValueHandling.Ignore;

                return settings;
            });
        }

        public async Task<decimal> GetTaxForOrder(Order order)
        {
            var jsonBody = JsonConvert.SerializeObject(new TaxRequest(order));

            var response = await apiClient.PostAsync("/v2/taxes", new StringContent(jsonBody, Encoding.UTF8, "application/json"));
            response.EnsureSuccessStatusCode();

            var x = JsonConvert.DeserializeObject<TaxResponse>(await response.Content.ReadAsStringAsync());

            return x.Tax.AmountToCollect;
        }

        public async Task<TaxRateInfo> GetTaxRates(string zipCode)
        {
            var response = await apiClient.GetAsync($"/v2/rates/{zipCode}");
            response.EnsureSuccessStatusCode();

            //var x = JsonConvert.DeserializeObject<Dictionary<string, string>>(await response.Content.ReadAsStringAsync());
            var deserializedResponse = JsonConvert.DeserializeObject<RateResponse>(await response.Content.ReadAsStringAsync());

            return deserializedResponse.Rate;
        }

        public async Task<TaxRateInfo> GetTaxRates(Address addr)
        {
            var queryParams = new Dictionary<string, string>
            {
                { "country", addr.Country },
                { "state", addr.State },
                { "city", addr.City },
                { "street", addr.Street }
            };

            var response = await apiClient.GetAsync($"/v2/rates/{addr.Zip}{CreateQueryString(queryParams)}");
            response.EnsureSuccessStatusCode();

            var deserializedResponse = JsonConvert.DeserializeObject<RateResponse>(await response.Content.ReadAsStringAsync());

            return deserializedResponse.Rate;
        }

        private static string CreateQueryString(Dictionary<string, string> parameters)
        {
            var encodedParams = parameters
                .Where(p => string.IsNullOrEmpty(p.Value))
                .Select(p => $"{p.Key}={HttpUtility.UrlEncode(p.Value)}");
            return $"?q={string.Join("&", encodedParams)}";
        }

        public void Dispose()
        {
            apiClient.Dispose();
        }
    }
}
