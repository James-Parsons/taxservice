﻿using System;
using System.Threading.Tasks;
using TaxService.DomainModels;

namespace TaxService
{
    public interface ITaxCalculator : IDisposable
    {
        public Task<decimal> GetTaxForOrder(Order order);
        public Task<TaxRateInfo> GetTaxRates(string zipCode);
        public Task<TaxRateInfo> GetTaxRates(Address addr);
    }
}
