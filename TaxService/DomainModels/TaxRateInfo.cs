﻿namespace TaxService.DomainModels
{
    public record TaxRateInfo {
        public decimal CountryRate { get; set; }
        public decimal StateRate  { get; set; }
        public decimal CityRate { get; set; }
        public decimal CombinedDistrictRate { get; set; }
        public decimal CombinedRate { get; set; }
        public bool FreightTaxable { get; set; }
    }
}
