﻿namespace TaxService.DomainModels
{
    public enum TaxSource
    {
        Origin,
        Destination
    }
}
