﻿namespace TaxService.DomainModels
{
    public enum ExcemptionType
    {
        Wholesale,
        Government,
        Marketplace,
        Other,
        NonExcempt
    }
}
