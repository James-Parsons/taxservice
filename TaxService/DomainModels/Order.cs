﻿using System.Collections.Generic;
using System.Linq;

namespace TaxService.DomainModels
{
    public class Order
    {
        public Address FromAddress { get; set; }
        public Address ToAddress { get; set; }
        public decimal? Amount { get; set; }
        public decimal Shipping { get; set; }
        public string CustomerId { get; set; }
        public ExcemptionType? ExcemptionType { get; set; }
        public IEnumerable<LineItem> LineItems { get; set; }

        public bool IsValid()
        {
            return !string.IsNullOrEmpty(ToAddress.Country)
                   && !string.IsNullOrEmpty(ToAddress.State)
                   && (Amount != null || (LineItems != null && LineItems.Any()));
        }
    }
}
