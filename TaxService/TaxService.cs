﻿using System;
using System.Threading.Tasks;
using TaxService.DomainModels;

namespace TaxService
{
    public class TaxService : IDisposable
    {
        public ITaxCalculator taxCalculator;

        public TaxService(ITaxCalculator taxCalculator)
        {
            this.taxCalculator = taxCalculator;
        }

        public Task<decimal> GetTaxForOrder(Order order) 
        {
            return taxCalculator.GetTaxForOrder(order);
        }
        public Task<TaxRateInfo> GetTaxRates(string zipCode)
        {
            return taxCalculator.GetTaxRates(zipCode);
        }

        public Task<TaxRateInfo> GetTaxRates(Address addr)
        {
            return taxCalculator.GetTaxRates(addr);
        }

        public void Dispose()
        {
            taxCalculator.Dispose();
        }
    }
}
