using NUnit.Framework;
using System.Collections.Generic;
using TaxService.DomainModels;

namespace TaxService.Tests
{
    public class OrderTests
    {
        //[SetUp]
        //public void Setup()
        //{
        //}

        [Test]
        public void ShouldBeValid()
        {
            var order = new Order
            {
                CustomerId = "test",
                ExcemptionType = ExcemptionType.NonExcempt,
                FromAddress = new Address
                {
                    Country = "US",
                    Zip = "33803",
                    City = "Lakeland",
                    State = "FL",
                },
                ToAddress = new Address
                {
                    Country = "US",
                    Zip = "33803",
                    City = "Lakeland",
                    State = "FL"
                },
                Shipping = 5.00M,
                LineItems = new List<LineItem>()
                {
                    new LineItem
                    {
                        Discount = 0.00M,
                        Id = "Item1",
                        Quantity = 2,
                        UnitPrice = 25.00M
                    },
                    new LineItem
                    {
                        Discount = 0.00M,
                        Id = "Item2",
                        Quantity = 1,
                        UnitPrice = 30.00M
                    }
                }
            };

            bool isValid = order.IsValid();
            Assert.IsTrue(isValid);
        }

        [Test]
        public void ShouldBeInvalidNoAmountOrLineItems()
        {
            var order = new Order
            {
                CustomerId = "test",
                ExcemptionType = ExcemptionType.NonExcempt,
                FromAddress = new Address
                {
                    Country = "US",
                    Zip = "33803",
                    City = "Lakeland",
                    State = "FL",
                },
                ToAddress = new Address
                {
                    Country = "US",
                    Zip = "33803",
                    City = "Lakeland",
                    State = "FL"
                },
                Shipping = 5.00M,
            };

            bool isValid = order.IsValid();
            Assert.IsFalse(isValid);
        }

        [Test]
        public void ShouldBeInvalidNoToCountry()
        {
            var order = new Order
            {
                Amount = 100.00M,
                CustomerId = "test",
                ExcemptionType = ExcemptionType.NonExcempt,
                FromAddress = new Address
                {
                    Country = "US",
                    Zip = "33803",
                    City = "Lakeland",
                    State = "FL",
                },
                ToAddress = new Address
                {
                    Zip = "33803",
                    City = "Lakeland",
                    State = "FL"
                },
                Shipping = 5.00M,
            };

            bool isValid = order.IsValid();
            Assert.IsFalse(isValid);
        }

        [Test]
        public void ShouldBeInvalidNoToState()
        {
            var order = new Order
            {
                Amount = 100.00M,
                CustomerId = "test",
                ExcemptionType = ExcemptionType.NonExcempt,
                FromAddress = new Address
                {
                    Country = "US",
                    Zip = "33803",
                    City = "Lakeland",
                    State = "FL",
                },
                ToAddress = new Address
                {
                    Country = "US",
                    Zip = "33803",
                    City = "Lakeland",
                },
                Shipping = 5.00M,
            };

            bool isValid = order.IsValid();
            Assert.IsFalse(isValid);
        }
    }
}