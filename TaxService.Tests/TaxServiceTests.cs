﻿using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaxService.DomainModels;

namespace TaxService.Tests
{
    public class TaxServiceTests
    {
        private Mock<ITaxCalculator> taxCalculatorMock;

        [SetUp]
        public void Setup()
        {
            taxCalculatorMock = new Mock<ITaxCalculator>(MockBehavior.Strict);
            taxCalculatorMock.Setup(m => m.GetTaxRates("00000")).ReturnsAsync(new TaxRateInfo
            {
                CountryRate = 0.0M,
                CityRate = 0.06M,
                StateRate = 0.01M,
                CombinedDistrictRate = 0.0M,
                CombinedRate = 0.07M,
                FreightTaxable = false
            });
        }

        [Test]
        public async Task TestGetRates()
        {
            var taxService = new TaxService(taxCalculatorMock.Object);
     
            var zip = "00000";

            var expected = new TaxRateInfo
            {
                CountryRate = 0.0M,
                CityRate = 0.06M,
                StateRate = 0.01M,
                CombinedDistrictRate = 0.0M,
                CombinedRate = 0.07M,
                FreightTaxable = false
            };
            var actual = await taxService.GetTaxRates(zip);

            Assert.That(actual, Is.EqualTo(expected));
        }

        [Test]
        public async Task TestGetTaxForOrderAmount()
        {
            taxCalculatorMock.Setup(m => m.GetTaxForOrder(
                It.Is<Order>(o => o.Amount == 5.00M && o.ExcemptionType == ExcemptionType.NonExcempt && o.ToAddress.Zip == "00000")
            )).ReturnsAsync(0.35M);
            var taxService = new TaxService(taxCalculatorMock.Object);

            var order = new Order
            {
                Amount = 5.00M,
                CustomerId = "Test",
                ExcemptionType = ExcemptionType.NonExcempt,
                FromAddress = new Address { City = "Test", Country = "US", State = "FL", Zip = "00000" },
                ToAddress = new Address { City = "Test", Country = "US", State = "FL", Zip = "00000" },
                Shipping = 5.00M
            };
            var rate = await taxService.GetTaxRates(order.ToAddress.Zip);

            var expected = order.Amount * rate.CombinedRate;
            var actual = await taxService.GetTaxForOrder(order);

            Assert.That(actual, Is.EqualTo(expected));
        }
         
        [Test]
        public async Task TestGetTaxForOrderLineItems()
        {
            taxCalculatorMock.Setup(m => m.GetTaxForOrder(
                It.Is<Order>(o => new LineItem[] {
                    new LineItem { Discount = 0.0M, Quantity = 3, UnitPrice = 3.00M },
                    new LineItem { Discount = 0.0M, Quantity = 1, UnitPrice = 1.00M }
                }.All(li => o.LineItems.Contains(li)))
            )).ReturnsAsync(0.70M);
            var taxService = new TaxService(taxCalculatorMock.Object);

            var order = new Order
            {
                CustomerId = "Test2",
                ExcemptionType = ExcemptionType.NonExcempt,
                FromAddress = new Address { City = "Test", Country = "US", State = "FL", Zip = "00000" },
                ToAddress = new Address { City = "Test", Country = "US", State = "FL", Zip = "00000" },
                LineItems = new LineItem[]
                {
                    new LineItem { Discount = 0.0M, Quantity = 3, UnitPrice = 3.00M },
                    new LineItem { Discount = 0.0M, Quantity = 1, UnitPrice = 1.00M }
                },
                Shipping = 5.00M
            };
            var rate = await taxService.GetTaxRates(order.ToAddress.Zip);

            var expected = order.LineItems.Select(i => i.UnitPrice * i.Quantity).Sum() * rate.CombinedRate;
            var actual = await taxService.GetTaxForOrder(order);

            Assert.That(actual, Is.EqualTo(expected));
        }
    }
}
